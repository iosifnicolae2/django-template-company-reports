"""{{ project_name }} URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include

from graphene_django.views import GraphQLView

from web import views
# TODO: update me

urlpatterns = [
    path('', views.home, name='home'),
    path('in_stock_books', views.in_stock_books, name='in_stock_books'),
    path('out_of_stock_books', views.out_of_stock_books, name='out_of_stock_books'),
    path('stock_vs_out_of_stock', views.stock_vs_out_of_stock, name='stock_vs_out_of_stock'),
    path('extract_products_details', views.extract_products_details, name='extract_products_details'),
    path('admin/', admin.site.urls),
    url(r'^explorer/', include('explorer.urls')),
    url(r'^graphql', GraphQLView.as_view(graphiql=True)),
]

# Mailo serve static files
from django.conf import settings
from django.conf.urls.static import static
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
