django==2.1
psycopg2-binary==2.7.7
gunicorn==19.9.0
django-sql-explorer==1.1.2
xlsxwriter==1.0.2
graphene-django==2.2.0
requests==2.21.0
