from django.contrib import admin

from web.models import Book as BookModel

# TODO: update me


@admin.register(BookModel)
class BookAdmin(admin.ModelAdmin):
    search_fields = ('title', 'description', 'excerpt',)
    list_display = ('title', 'page_count')
