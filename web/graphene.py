import graphene

from graphene_django import DjangoObjectType

from web.models import Book


# TODO: update me

class GrapheneBook(DjangoObjectType):
    class Meta:
        model = Book


class Query(graphene.ObjectType):
    books = graphene.List(GrapheneBook)

    def resolve_books(self, info):
        return Book.objects.all()


schema = graphene.Schema(query=Query)
