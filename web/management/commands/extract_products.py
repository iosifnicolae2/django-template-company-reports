import requests
from django.core.management.base import BaseCommand
from django.utils.crypto import random

from web.models import Book

BOOKS_API = "https://fakerestapi.azurewebsites.net/api/Books"


# TODO: update me
class Command(BaseCommand):
    help = 'Update database with available books.'

    def handle(self, *args, **options):
        # Drop all the products
        Book.objects.all().delete()

        # Download books information
        response = requests.get(BOOKS_API)
        books = response.json()

        # Save books into database
        for book in books[:20]:
            Book.objects.create(
                title=book['Title'],
                description=book['Description'],
                excerpt=book['Excerpt'],
                page_count=book['PageCount'],
                in_stock=random.choice([True, False, True]),
            )
        print('Books have been imported successfully!')
