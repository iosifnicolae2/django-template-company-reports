from django.db import models


# TODO: update me
class Book(models.Model):
    title = models.TextField()
    description = models.TextField()
    excerpt = models.TextField()
    page_count = models.IntegerField()
    in_stock = models.BooleanField()

    def __str__(self):
        return self.title
