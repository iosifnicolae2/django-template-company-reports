from django.shortcuts import render, redirect
from django.core.management import call_command

# TODO: update me
from web.models import Book


def extract_products_details(request):
    call_command('extract_products')
    return redirect(to='home')


def home(request):
    return render(request, 'home.html')


def in_stock_books(request):
    books = Book.objects.filter(in_stock=True).values()
    return render(
        request,
        'in_stock_books.html',
        {
            'books': books,
        }
    )


def out_of_stock_books(request):
    books = Book.objects.filter(in_stock=True).values()
    return render(
        request,
        'out_of_stock_books.html',
        {
            'books': books,
        }
    )


def stock_vs_out_of_stock(request):
    in_stock = Book.objects.filter(in_stock=True).count()
    out_of_stock = Book.objects.filter(in_stock=False).count()
    return render(
        request,
        'stock_vs_out_of_stock.html',
        {
            'in_stock_books': in_stock,
            'out_of_stock_books': out_of_stock,
        }
    )
